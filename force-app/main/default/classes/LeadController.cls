public with sharing class LeadController{
public Lead lead; 
public Lead leadSearch = new Lead();
public List<Lead> leads = new List<Lead>();
public List<Lead> getLeadSearchResult(){
    return (leads.size() > 0) ? leads : Null;
}    
public void searchLead(){
    leads  = [SELECT firstname,lastname,email,phone,leadsource,status FROM Lead WHERE status=:leadSearch.status OR leadsource =:leadSearch.leadsource];
}      
public void queryLead(){
    try{
    Id leadId=System.currentPageReference().getParameters().get('leadId');
    lead = [SELECT firstname,lastname,email,phone,leadsource,status FROM Lead WHERE ID =:leadId];
    } 
    catch(exception e){
        System.debug(e.getMessage());
    }
}    
public Lead getLeadDetail(){
    return (lead == Null) ? Null : lead;
    } 
public Lead getLeadSearch(){return leadSearch;}
public void saveLead(){
    try{   
    update lead;        
    }
    catch(Exception e){
        System.debug(e.getMessage());
    }
}  

@AuraEnabled(cacheable=true)
public static List<LeadWrapper> findLeads( String status, String source)
{
    
    String query = 'SELECT Id, firstname, lastname, leadsource ,status FROM Lead WHERE';
    query = String.isNotBlank(status) ? query + ' Status LIKE \'%' + status + '%\' AND ' : query;
    query = String.isNotBlank(source) ? query + ' LeadSource LIKE \'%' + source + '%\' AND ': query;
    query = query.removeEnd('AND ');
    LeadWrapper leadWrapper;
    List<LeadWrapper> leadWrappers = new List<LeadWrapper>();
    for(Lead lead: Database.query(query)){
        leadWrapper = new LeadWrapper(lead.ID, '/lightning/r/Lead/'+lead.Id+'/view' ,lead.FirstName, lead.LastName, lead.status,lead.leadSource);
        leadWrappers.add(leadWrapper);
    }
    return leadWrappers;
}

public class LeadWrapper{
    @AuraEnabled public string leadId{get;set;}
    @AuraEnabled public string leadURL{get;set;}
    @AuraEnabled public string firstName{get;set;}
    @AuraEnabled public string lastname{get;set;}
    @AuraEnabled public string status{get;set;}
    @AuraEnabled public string source{get;set;}
    public LeadWrapper(String leadId,String leadURL, String firstName, string lastName, string status, string source){
        this.leadId = leadId;
        this.leadURL = leadURL;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.source = source;
    }
}  
}