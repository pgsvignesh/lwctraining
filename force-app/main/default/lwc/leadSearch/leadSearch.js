import { LightningElement, wire } from "lwc";
import { getPicklistValues } from "lightning/uiObjectInfoApi";
import STATUS_FIELD from "@salesforce/schema/Lead.Status";
import SOURCE_FIELD from "@salesforce/schema/Lead.LeadSource";
import { CurrentPageReference } from "lightning/navigation";
import { fireEvent } from "c/pubsub";

export default class LeadSearch extends LightningElement {
  @wire(CurrentPageReference) pageRef;

  @wire(getPicklistValues, {
    recordTypeId: "012000000000000AAA",
    fieldApiName: STATUS_FIELD
  })
  availableStatus;
  selectedStatus = "";

  @wire(getPicklistValues, {
    recordTypeId: "012000000000000AAA",
    fieldApiName: SOURCE_FIELD
  })
  availableSource;
  selectedSource = "";

  handleChange(event) {
    if (event.target.name === "status") {
      this.selectedStatus = event.target.value;
    }
    if (event.target.name === "source") {
      this.selectedSource = event.target.value;
    }
  }

  handleSearch(event) {
    let eventData = {
      status: this.selectedStatus,
      source: this.selectedSource
    };
    fireEvent(this.pageRef, "searchKeyChange", eventData);
  }
}
